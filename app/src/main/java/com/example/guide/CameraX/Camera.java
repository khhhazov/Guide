package com.example.guide.CameraX;
import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.util.Size;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageProxy;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.guide.R;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.ExecutionException;


public class Camera extends AppCompatActivity{
    private static final int PERMISSION_REQUEST_CAMERA = 123; //ID камеры
    private ImageView preview;
    ListenableFuture<ProcessCameraProvider> cameraProviderFuture;
    YUVtoRGB translator = new YUVtoRGB();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_layout);
        preview = findViewById(R.id.preview);
        //Запрос на использование камеры
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA},
                    PERMISSION_REQUEST_CAMERA);
        } else {
            initializeCamera();
        }
    }
    //Обработка запроса на использование камеры
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CAMERA && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            initializeCamera();
        }
    }
    //Запуск самой камеры
    private void initializeCamera() {
        cameraProviderFuture = ProcessCameraProvider.getInstance(this); //Инициализация провайдера камеры
        cameraProviderFuture.addListener(new Runnable() {
            @Override
            public void run() { //Этот колбэк используется для того, чтобы программа понимала, что делать дальше после окончания инициализации провайдера камеры
                try {
                    ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                    ImageAnalysis imageAnalysis = new ImageAnalysis.Builder() //Сценарий анализа изображения
                            .setTargetResolution(new Size(1024, 768)) //Разрешение
                            .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST) //Сохранение только последнего изображения
                            .build();
                    CameraSelector cameraSelector = new CameraSelector.Builder() //Селектор камеры
                            .requireLensFacing(CameraSelector.LENS_FACING_BACK) //Задняя камера
                            .build();
                    imageAnalysis.setAnalyzer(ContextCompat.getMainExecutor(Camera.this),
                            new ImageAnalysis.Analyzer() { //Данные из изображения для обработки
                                @Override
                                public void analyze(@NonNull ImageProxy image) {
                                    @SuppressLint("UnsafeOptInUsageError") Image img = image.getImage();
                                    Bitmap bitmap = translator.translateYUV(img, Camera.this); //Конвертирует в нормальный формат
//Далее делает из цветного изображения Ч/Б, чтобы отключить, нужно закоментить
                                    int size = bitmap.getWidth() * bitmap.getHeight();
                                    int[] pixels = new int[size];
                                    bitmap.getPixels(pixels, 0, bitmap.getWidth(), 0, 0,
                                            bitmap.getWidth(), bitmap.getHeight());
                                    for (int i = 0; i < size; i++) {
                                        int color = pixels[i];
                                        int r = color >> 16 & 0xff;
                                        int g = color >> 8 & 0xff;
                                        int b = color & 0xff;
                                        int gray = (r + g + b) / 3;
                                        pixels[i] = 0xff000000 | gray << 16 | gray << 8 | gray;
                                    }
//Следующие команды нужны для нормального отображения изображения
                                    bitmap.setPixels(pixels, 0, bitmap.getWidth(), 0, 0,
                                            bitmap.getWidth(), bitmap.getHeight());
                                    preview.setRotation(image.getImageInfo().getRotationDegrees());
                                    preview.setImageBitmap(bitmap);
                                    image.close();
                                }
                            });
                    cameraProvider.bindToLifecycle(Camera.this, cameraSelector, imageAnalysis); //Для возможности использования нескольких сценариев
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, ContextCompat.getMainExecutor(this));
    }
}
