package com.example.guide.retrofit;

import com.example.guide.model.User;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class NetworkService {
    private static final String BASE_URL = "http://25.94.93.7:8081/";
    OkHttpClient client = new OkHttpClient();
    ObjectMapper mapper = new ObjectMapper();

    public <B> String post(String path, B body, Map<String, Object> params) {
        try {
            RequestBody serializedBody = RequestBody.create(MediaType.parse("application/json"), mapper.writeValueAsString(body));
            Request request = new Request.Builder()
                    .url(BASE_URL + path + "?" + params.entrySet().stream()
                            .map(x -> new StringBuilder(x.getKey()).append("=").append(x.getValue()))
                            .collect(
                                    Collectors.joining("&"))
                    )
                    .post(serializedBody)
                    .build();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();
            if (responseBody != null) {
                return responseBody.string();
            } else {
                return null;
            }

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String get(String path, Map<String, Object> params) {
        try {
            Request request = new Request.Builder()
                    .url(BASE_URL + path + "?" + params.entrySet().stream()
                            .map(x -> new StringBuilder(x.getKey()).append("=").append(x.getValue()))
                            .collect(
                                    Collectors.joining("&")))
                    .build();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();
            if (responseBody != null) {
                return responseBody.string();
            } else {
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

//    public byte[] getRequest(String login, String password) throws JsonProcessingException {
//        final String GET_URL = "http://25.67.120.1:8081/users/" + login + "/" + password;
//
//        OkHttpClient client = new OkHttpClient();
//        okhttp3.Request request = new okhttp3.Request.Builder()
//                .url(GET_URL)
//                .build();
//
//        try (okhttp3.Response response = client.newCall(request).execute()) {
//            return response.body().bytes();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return null;
//    }
}

