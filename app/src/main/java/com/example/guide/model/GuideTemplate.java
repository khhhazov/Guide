package com.example.guide.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GuideTemplate {
    @SerializedName("guideName")
    @Expose
    private String guideName;
    @SerializedName("guide")
    @Expose
    private String guide;

    public GuideTemplate(String nameGuide, String guide){
        this.guideName = nameGuide;
        this.guide = guide;
    }

    public String getGuideName() {
        return guideName;
    }

    public void setGuideName(String guideName) {
        this.guideName = guideName;
    }

    public String getGuide() {
        return guide;
    }

    public void setGuide(String guide) {
        this.guide = guide;
    }
}
