package com.example.guide.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Guide {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("guideName")
    @Expose
    private String guideName;
    @SerializedName("guide")
    @Expose
    private String guide;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGuideName() {
        return guideName;
    }

    public void setGuideName(String guideName) {
        this.guideName = guideName;
    }

    public String getGuide() {
        return guide;
    }

    public void setGuide(String guide) {
        this.guide = guide;
    }
}
