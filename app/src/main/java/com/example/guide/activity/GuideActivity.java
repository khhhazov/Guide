package com.example.guide.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.guide.R;
import com.example.guide.api.GuidesApi;
import com.example.guide.model.Guide;

import java.util.concurrent.ExecutionException;

public class GuideActivity extends AppCompatActivity {
    TextView guideName;
    TextView guide;
    String guideInfo;
    String setName;
    GuideInfo gi;
    String get;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guide_activity);
        init();
    }

    public void init(){
        guideName = (TextView) findViewById(R.id.GuideName);
        guide = (TextView) findViewById(R.id.Guide);
        setName = getIntent().getStringExtra("guideName");
        gi = new GuideInfo();
        gi.execute();

        try {
            get = gi.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

//        String setGuide = getIntent().getStringExtra("guide");
        guideName.setText(setName);
        guide.setText(get);
    }

    class GuideInfo extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String[] args){
            GuidesApi api = new GuidesApi();
            Guide guideGet = new Guide();
            guideGet = api.getGuide(setName);
            guideInfo = guideGet.getGuide();
            return guideInfo;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
        }
    }
}
