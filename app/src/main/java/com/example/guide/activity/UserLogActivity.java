package com.example.guide.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.guide.R;
import com.example.guide.api.UsersApi;
import com.example.guide.model.User;

import java.util.concurrent.ExecutionException;

public class UserLogActivity extends AppCompatActivity {
    EditText logLogin;
    EditText logPassword;
    String login;
    String password;
    String getLogin = null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_layout);
        init();
    }

    public void init(){
        logLogin = (EditText) findViewById(R.id.logLogin);
        logPassword = (EditText) findViewById(R.id.logPassword);
    }

    public void onClickLogIn(View view){
        LogInLoginAndPassword lilap;
        lilap = new LogInLoginAndPassword();
        lilap.execute();
        User getUser = null;

        try {
            getUser = lilap.get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        if (getUser != null){
            getLogin = getUser.getLogin();
        }

        if (login.equals(getLogin)){
            Intent log = new Intent(this, MainActivity.class);
            startActivity(log);
        }
        else{
            Intent reg = new Intent(this, UserRegActivity.class);
            startActivity(reg);
        }
    }

    class LogInLoginAndPassword extends AsyncTask <Void, User, User> {
        @Override
        protected User doInBackground(Void... args){
            login = logLogin.getText().toString();
            password = logPassword.getText().toString();

            UsersApi api = new UsersApi();
            User user = new User();
            user = api.getUser(login, password);
            return user;
        }

        protected void onPostExecute(User result) {
            super.onPostExecute(result);
        }
    }
}
