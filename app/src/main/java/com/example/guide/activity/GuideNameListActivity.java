package com.example.guide.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.guide.R;
import com.example.guide.api.GuidesApi;
import com.example.guide.model.Guide;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class GuideNameListActivity extends AppCompatActivity {
    String result[];
    String strText;
    ListView nameList;
    ArrayAdapter<String> adapter;
    ArrayList<String> guideName;
    List<Guide> list;
    String[] name;
    GetAllGuideName gagm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_guide_activity);

        gagm = new GetAllGuideName();
        gagm.execute();
        viewList();
        clickItem();
    }

    public void init(){
    }

    public void viewList() {
        nameList = (ListView) findViewById(R.id.listView);

        try {
            result = gagm.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, result);
        nameList.setAdapter(adapter);

    }

    public void clickItem(){
        nameList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View itemClicked, int i, long l) {
                TextView textView = (TextView) itemClicked;
                strText = textView.getText().toString();

                Intent intent = new Intent(getApplicationContext(),GuideActivity.class);
                intent.putExtra("guideName", strText);
                intent.putExtra("guide", strText);
                startActivity(intent);
            }
        });
    }

    class GetAllGuideName extends AsyncTask<String, String[], String[]> {

        //Создание пользователя
        @Override
        protected String[] doInBackground(String[] args) {
            GuidesApi api = new GuidesApi();

            list = api.getAllGuides(); // get request

            name = new String[list.size()];
            guideName = new ArrayList<>();
            int i = 0;
            for (Guide guide : list) {
                name[i] = guide.getGuideName();
                i++;
            }

            return name;
        }

        @Override
        protected void onPostExecute(String[] result){
            super.onPostExecute(result);
        }
    }
}
