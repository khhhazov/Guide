package com.example.guide.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.guide.R;
import com.example.guide.api.GuidesApi;
import com.example.guide.model.Guide;
import com.example.guide.model.GuideTemplate;

public class AddGuideActivity extends AppCompatActivity {
    Button addGuide;
    EditText edGuideName;
    EditText edGuide;
    GuidesApi api = new GuidesApi();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_guide);
        init();
    }

    public void init(){
        addGuide = (Button) findViewById(R.id.bAddGuide);
        edGuideName = (EditText) findViewById(R.id.edGuideName);
        edGuide = (EditText) findViewById(R.id.edGuide);
    }

    public void onClickAddGuide(View view){
        new CreateNewGuide().execute();
    }

    class CreateNewGuide extends AsyncTask<String, String, String> {

        //Создание пользователя
        protected String doInBackground(String[] args) {
            String nameGuide = edGuideName.getText().toString();
            String guide = edGuide.getText().toString();

            GuideTemplate template = new GuideTemplate(nameGuide, guide);
            Guide newGuide = api.createGuide(template); //post request

            return null;
        }
    }
}
