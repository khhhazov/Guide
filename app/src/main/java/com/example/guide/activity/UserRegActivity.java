package com.example.guide.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.guide.R;
import com.example.guide.api.UsersApi;
import com.example.guide.model.User;
import com.example.guide.model.UserTemplate;

public class UserRegActivity extends AppCompatActivity {
    EditText regLogin;
    EditText regPassword;
    TextView getReq;
    UsersApi api = new UsersApi();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reg_layout);
        init();
    }

    private void init(){
        regLogin = (EditText) findViewById(R.id.regLogin);
        regPassword = (EditText) findViewById(R.id.regPassword);
    }

    public void onClickLogIn(View view){
        Intent intent = new Intent(this, UserLogActivity.class);
        startActivity(intent);
    }

    public void onClickSignUp(View view) {
        new CreateNewUser().execute();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    class CreateNewUser extends AsyncTask<String, String, String>{

        //Создание пользователя
        protected String doInBackground(String[] args) {
            String login = regLogin.getText().toString();
            String password = regPassword.getText().toString();

            UserTemplate template = new UserTemplate(login, password);
            User user = api.createUser(template); //post request
//            User user = api.getUser("lofin", "ggddw"); // get request
//            getReq.setText(user.getLogin());

            return null;
        }
    }
}
