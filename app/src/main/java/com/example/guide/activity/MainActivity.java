package com.example.guide.activity;

import static android.telephony.AvailableNetworkInfo.PRIORITY_HIGH;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import com.example.guide.CameraX.Camera;
import com.example.guide.R;

public class MainActivity extends AppCompatActivity {
    NotificationManager notificationManager;  //менеджер уведомлений
    int NOTIFY_ID = 1; //разделение для нескольких уведомлений в одном прил.
    String CHANNEL_ID = "CHANNEL_ID"; //переменная
    ImageButton b1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        send();
    }

    public void send (){
        b1=findViewById(R.id.Send);  //Нахождение кнопки по её ID
        notificationManager = (NotificationManager) //для получения самих уведомлений
                getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                b1.setOnClickListener(new View.OnClickListener() { //обработчик нажатия на кнопку
                    @Override
                    public void onClick(View v) {
                        //флаги для очистки или создания новых уведомлений
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        //Строитель уведомлений
                        NotificationCompat.Builder notificationBuilder =
                                new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                                        .setAutoCancel(false)
                                        .setSmallIcon(R.drawable.ic_launcher_foreground) //обычная иконка приложения
                                        .setWhen(System.currentTimeMillis()) //передача времени в милисекундах
                                        .setContentIntent(pendingIntent)
                                         //текст уведомления
                                        .setContentTitle("Приложение Guide ИКБО-12-20 (Хазов Семён)")
                                        .setContentText("Здесь явно что-то интересное")
                                        .setPriority(PRIORITY_HIGH); //приоритет
                        //Дальше нужно для работы на версиях API 26 и выше
                        createChannelIfNeeded(notificationManager);
                        notificationManager.notify(NOTIFY_ID, notificationBuilder.build());
                    }
                });
    }

    public void createChannelIfNeeded(NotificationManager manager) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_ID, NotificationManager.IMPORTANCE_DEFAULT);
            manager.createNotificationChannel(notificationChannel);
        }
    }

    public void onClickImage(View view){
        Intent intent = new Intent(this, Camera.class);
        startActivity(intent);
    }

    public void onClickSettings(View view){
        Intent intent = new Intent(this, UserRegActivity.class);
        startActivity(intent);
    }

    public void onClickGuide(View view){
        Intent intent = new Intent(this, GuideNameListActivity.class);
        startActivity(intent);
    }

    public void onClickAddGuide(View view){
        Intent intent = new Intent(this, AddGuideActivity.class);
        startActivity(intent);
    }
}