package com.example.guide.api;

import com.example.guide.model.Guide;
import com.example.guide.model.GuideTemplate;
import com.example.guide.retrofit.NetworkService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Collections;
import java.util.List;

public class GuidesApi {
    NetworkService networkService = new NetworkService();
    ObjectMapper mapper = new ObjectMapper();

    public Guide getGuide(String guideName) {
        String json = networkService.get("guide/" + guideName, Collections.emptyMap());
        try {
            return mapper.readValue(json, new TypeReference<Guide>() {});
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Guide> getAllGuides() {
        String json = networkService.get("guide", Collections.emptyMap());
        try {
            return mapper.readValue(json, new TypeReference<List<Guide>>() {});
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Guide createGuide(GuideTemplate guideTemplate) {
        String json = networkService.post("guide/create", guideTemplate, Collections.emptyMap());
        try {
            return mapper.readValue(json, new TypeReference<Guide>() {});
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
