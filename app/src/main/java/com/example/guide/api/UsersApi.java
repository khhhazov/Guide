package com.example.guide.api;

import com.example.guide.model.User;
import com.example.guide.model.UserTemplate;
import com.example.guide.retrofit.NetworkService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Collections;

public class UsersApi {
    NetworkService networkService = new NetworkService();
    ObjectMapper mapper = new ObjectMapper();

    public User getUser(String login, String password) {
        String json = networkService.get("user/" + login + "/" + password, Collections.emptyMap());
        try {
            return mapper.readValue(json, new TypeReference<User>() {});
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public User createUser(UserTemplate userTemplate) {
        String json = networkService.post("user/create", userTemplate, Collections.emptyMap());
        try {
            return mapper.readValue(json, new TypeReference<User>() {});
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

//    Request req = new Request.Builder().url("http://25.67.120.1:8081/users/" + login + "/" + password).build();
//        try {
//            String response = client.newCall(req).execute().body().string();
//            return mapper.readValue(response, new TypeReference<List<UsersGet>>() {});
//        } catch (IOException e) {
//            e.printStackTrace();
//            return Collections.emptyList();
//        }
}
